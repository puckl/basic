eComStyle.de::basic
==========================

#### Composer name
`ecs/basic`


#### Shopversionen
`OXID eShop CE/PE 6`


#### Installation Produktiv-Umgebung
`composer require ecs/basic --update-no-dev`


#### Installation Dev-Umgebung
`composer require ecs/basic`
